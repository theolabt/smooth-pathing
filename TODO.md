TO-DOs
===

- I don't like how the Update / Draw is made in CurveDisplay
I would like to define it properly, maybe with an Interface or an Abstract class, this is way to easy to break for no reason

- ColorTree is basicaly a LerpComposite but 3D
It should be made as such

- Modify the Editor to be able to do crtl+z to go back a step
THis means that actions needs to be defined properly, savable in a list
and then reversed

- Vectors Display
With the ability to update the display when modified with an addition as the composition of the vectors (Chasles relation)

- Editor should have something to display information
like the help for example
but also the display the interaction with the curve
like if a point can be moved or is being moved
a futur point on a line
things like that
