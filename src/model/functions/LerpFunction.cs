using splines.src.model.space;

namespace splines.src.model.functions;

abstract class Lerp<V> : Function<double, V> where V : Vector {
    public V PointA { get; }
    public V PointB { get; }

    public Lerp(V pointA, V pointB, Variable<double> variable) : base(variable) {
        if(variable.Values[0] < 0 || variable.Values[^1] > 1) {
            throw new System.Exception("Variable must have values between 0 and 1 for a lerp function");
        }

        this.PointA = pointA;
        this.PointB = pointB;
    }
}

class Lerp2D : Lerp<Vector2D> {
    public Lerp2D(Vector2D pointA, Vector2D pointB, Variable<double> variable) : base(pointA, pointB, variable) {}

    public override Vector2D Call(double t) {
        return Vector2D.Lerp(PointA, PointB, t).Round(2);
    }
}

class CompositeLerp2D : CompositeFunction<double, Vector2D> {
    public CompositeLerp2D(Function<double, Vector2D> lerpA, Function<double, Vector2D> lerpB) : base(lerpA, lerpB) {}

    public override Vector2D Call(double t) {
        Lerp2D lerp = new(LeftFunction.Call(t), RightFunction.Call(t), this.VariableX);

        return (Vector2D)lerp.Call(t);
    }
}