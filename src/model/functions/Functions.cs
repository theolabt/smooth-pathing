using splines.src.model.space;

namespace splines.src.model.functions;


//* Functions -- abstract classes definition
abstract class Function<X, Y> { // X => Y
    public Variable<X> VariableX { get; }
    protected List<Y>? values;

    public List<Y> Values { // simulates a Lazy set up. Since GenerateValues is also public, it should be used after early in the code, this set up is mainly a security measure
        get {
            if(values == null)
                GenerateValues();

            return values!;
        }
    }

    public Function(Variable<X> variable) {
        this.VariableX = variable;

        values = null;
    }

    public virtual void GenerateValues() {
        List<Y> values = new();
        foreach(X x in VariableX.Values) {
            values.Add(Call(x));
        }

        this.values = values;
    }

    abstract public Y Call(X x);
}

abstract class CurveFunction<X, V> : Function<X, V> where V : Vector {
    protected readonly List<V> buildingPoints;

    public List<V> BuildingPoints {
        get => buildingPoints;
    }

    public CurveFunction(List<V> buildingPoints, Variable<X> variable) : base(variable) {
        this.buildingPoints = buildingPoints;
    }

    public void InsertPoint(V point, int index) {
        if(index < 0 || index > buildingPoints.Count) {
            throw new System.Exception("Index out of range");
        }

        if(index == buildingPoints.Count) {
            buildingPoints.Add(point);
        }
        else {
            buildingPoints.Insert(index, point);
        }

        GenerateValues();
    }

    public void UpdatePoint(V point, int index) {
        if(index < 0 || index > buildingPoints.Count) {
            throw new System.Exception("Index out of range");
        }

        BuildingPoints[index] = point;

        GenerateValues();
    }

    public virtual void DeletePoint(int index) {
        if(index < 0 || index > buildingPoints.Count) {
            throw new System.Exception("Index out of range");
        }

        BuildingPoints.RemoveAt(index);

        GenerateValues();
    }
}

abstract class CompositeFunction<X, Y> : Function<X, Y> {
    public Function<X, Y> LeftFunction { get; private set; }
    public Function<X, Y> RightFunction { get; private set; }

    public CompositeFunction(Function<X, Y> leftFunction, Function<X, Y> rightFunction) : base(leftFunction.VariableX) {
        if(leftFunction.VariableX != rightFunction.VariableX) {
            throw new System.Exception("Both functions must have the same variable");
        }
        this.LeftFunction = leftFunction;
        this.RightFunction = rightFunction;
    }
}

//* Test functions

class SquareFunction : Function<double, double> {
    public SquareFunction(Variable<double> variable) : base(variable) {}

    public override double Call(double x) {
        return x * x;
    }
}
