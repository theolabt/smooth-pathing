using Raylib_cs;

using splines.src.model.space;
using splines.src.model.functions;

namespace splines.src.model.curves;

using lerpFunction = splines.src.model.functions.Function<double, splines.src.model.space.Vector2D>;

class BezierDeCasteljau : CurveFunction<double, Vector2D> {
    private lerpFunction finalLerp;

    public lerpFunction FinalLerp {
        get => finalLerp;
    }

    public BezierDeCasteljau(Variable<double> variable) : base(new(), variable) {
        this.buildingPoints.AddRange( new List<Vector2D> {
            new(Raylib.GetScreenWidth() / 4, Raylib.GetScreenHeight() / 2),
            new(Raylib.GetScreenWidth() *3 / 4, Raylib.GetScreenHeight() / 2)
        });

        finalLerp = BuildIntermediateLerps();
    }

    public BezierDeCasteljau(List<Vector2D> buildingPoints, Variable<double> variable) : base(buildingPoints, variable) {
        if(buildingPoints.Count < 2) {
            throw new System.Exception("A Bezier curve must have at least 2 points");
        }

        finalLerp = BuildIntermediateLerps();
    }

    public override void GenerateValues() {
        finalLerp = BuildIntermediateLerps();

        base.GenerateValues();
    }

    public override void DeletePoint(int index) {
        if(buildingPoints.Count == 2) {
            Console.WriteLine("Can't have less than 2 points");
            return;
        }
        
        base.DeletePoint(index);
    }

    private lerpFunction RecursivelyBuildLerps(List<lerpFunction> lerps) {
        if(lerps.Count == 1) {
            return lerps[0];
        }


        List<lerpFunction> newLerps = new();

        for(int i = 0; i < lerps.Count - 1; i++) {
            newLerps.Add(new CompositeLerp2D(lerps[i], lerps[i + 1]));
        }

        return RecursivelyBuildLerps(newLerps);
    }

    private lerpFunction BuildIntermediateLerps() {
        List<lerpFunction> lerps = new();

        for(int i = 0; i < buildingPoints.Count - 1; i++) {
            lerps.Add(new Lerp2D(buildingPoints[i], buildingPoints[i + 1], this.VariableX));
        }

        return RecursivelyBuildLerps(lerps);
    }

    public override Vector2D Call(double t) {
        return finalLerp.Call(t);
    }
}