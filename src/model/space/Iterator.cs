using Raylib_cs;

namespace splines.src.model.space;

abstract class Iterator<T> {
    protected readonly Variable<T> variable; // the thing we are iterating over

    public T CurrentValue {
        get => variable.CurrentValue;
    }

    public Iterator(Variable<T> variable) {
        this.variable = variable;
    }

    abstract public bool UpdateValue(float deltaTime);
}

class TimeIterator<T> : Iterator<T> {
    protected float currentTime;
    protected float frameRate;

    public TimeIterator(Variable<T> variable, int FPS) : base(variable) {
        currentTime = 0;
        frameRate = 1f / FPS;
    }

    public override bool UpdateValue(float deltaTime) {
        // Console.WriteLine("TimeIterator");

        if(variable.HasNext()) {
            currentTime += deltaTime;
            if(currentTime >= frameRate) {
                currentTime = 0;
                return variable.Next();
            }
            return true;
        }
        return false;
    }
}

class LoopingTimeIterator<T> : TimeIterator<T> { 
    public LoopingTimeIterator(Variable<T> variable, int FPS) : base(variable, FPS) {}

    public override bool UpdateValue(float deltaTime) {
        // Console.WriteLine("LoopingTimeIterator");

        if(!base.UpdateValue(deltaTime)) {
            currentTime += deltaTime;
            if(currentTime >= frameRate) {
                currentTime = 0;
                variable.Reset();
            }
        }
        return true;
    }
}


class KeyboardIterator<T> : Iterator<T> {
    public KeyboardIterator(Variable<T> variable) : base(variable) {}

    private bool MovementDown() {
        if(Raylib.IsKeyDown(KeyboardKey.Right)) {
            return variable.Next();
        }
        if(Raylib.IsKeyDown(KeyboardKey.Left)) {
            return variable.Previous();
        }
        return true;
    }

    private bool MovementPressed() {
        if(Raylib.IsKeyPressed(KeyboardKey.Right)) {
            return variable.Next();
        }
        if(Raylib.IsKeyPressed(KeyboardKey.Left)) {
            return variable.Previous();
        }
        return true;
    }

    public override bool UpdateValue(float deltaTime) {
        // Console.WriteLine("IteratorKeyboard");

        if(Raylib.IsKeyDown(KeyboardKey.LeftShift)) {
            MovementPressed();

            if(Raylib.IsKeyPressed(KeyboardKey.Up)) {
                variable.CurrentIndex = variable.Values.Count/2;
            }
        }
        else {
            MovementDown();

            if(Raylib.IsKeyPressed(KeyboardKey.Down)) {
                variable.Reset();
            }
            if(Raylib.IsKeyPressed(KeyboardKey.Up)) {
                variable.CurrentIndex = variable.Values.Count - 1;
            }
        }
        return true;
    }
}