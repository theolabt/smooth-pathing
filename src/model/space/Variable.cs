using System;

namespace splines.src.model.space;

class Variable<T> {
    private int currentIndex;
    private readonly List<T> values; 

    // Properties
    public T CurrentValue {
        get => values[currentIndex];
    }
    public int CurrentIndex {
        get => currentIndex;
        set {
            if(value < 0 || value >= values.Count) {
                throw new IndexOutOfRangeException("Index out of range");
            }
            currentIndex = value;
        }
    }
    public List<T> Values {
        get => values;
    }

    // Constructors
    public Variable(List<T> valueList) {
        values = valueList;
        if(values.Count == 0) {
            throw new Exception("Variable must have at least one value");
        }
        currentIndex = 0;
    }

    // Operations
    public bool HasNext() {
        return currentIndex < values.Count - 1;
    }
    public bool HasPrevious() {
        return currentIndex > 0;
    }

    public bool Next() {
        if(!HasNext()) {
            return false;
        }
        currentIndex++;
        return true;
    }
    public bool Previous() {
        if(!HasPrevious()) {
            return false;
        }
        currentIndex--;
        return true;
    }

    public void Reset() {
        currentIndex = 0;
    }


    // the Equality operators don't check the content of the Objects, but if they are actually the SAME object
    // which is the correct behavior for this class
    public static bool operator ==(Variable<T> a, Variable<T> b) {
        return ReferenceEquals(a, b);
    }
    public static bool operator !=(Variable<T> a, Variable<T> b) {
        return !ReferenceEquals(a, b);
    }
    public override bool Equals(object? obj) {
        return ReferenceEquals(this, obj);
    }
    public override int GetHashCode() {
        return System.Runtime.CompilerServices.RuntimeHelpers.GetHashCode(this);
    }

    public override string ToString() {
        return $"{{Variable<{typeof(T).Name}>: {CurrentValue}, [{Values[0]}, {Values[^1]}], {CurrentIndex}/{Values.Count}}}";
    }
}


class DoubleVariable : Variable<double> {
    public DoubleVariable(double A, double B, int nbSteps) : base(GenerateList(A, B, nbSteps)) {}

    private static List<double> GenerateList(double A, double B, int nbSteps) {
        List<double> list = new();

        if(A > B) {
            (B, A) = (A, B);
        }

        double step = (B - A) / nbSteps;

        for (double i = A; i <= B; i += step) {
            list.Add(Math.Round(i*10*nbSteps)/(10*nbSteps)); 
            // REVIEW This is a possible solution to avoid floating point errors, check back in later to see if this is a good one
        }
        if(list[^1] > B) {
            list.RemoveAt(list.Count - 1);
        }
        if(list[^1] != B) {
            list.Add(B);
        }

        return list;
    }
}