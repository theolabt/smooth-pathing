using Raylib_cs;

using splines.src.model.functions;

namespace splines.src.model.space;

class Editor {
    protected CurveFunction<double, Vector2D> curve;
    public bool Editable { get; set; }

    private List<Lerp2D> buildingPointsLines;
    private int movingPoint;
    private float tolerance;

    public Editor(CurveFunction<double, Vector2D> curve) {
        this.curve = curve;
        this.Editable = true;

        this.buildingPointsLines = BuildLines();
        movingPoint = -1;
        tolerance = 10;
    }

    private List<Lerp2D> BuildLines() {
        List<Lerp2D> buildingPointsLines = new();

        for(int i = 0; i < curve.BuildingPoints.Count - 1; i++) {
            DoubleVariable variableT = new DoubleVariable(0, 1, (int)Vector2D.Distance(curve.BuildingPoints[i], curve.BuildingPoints[i+1]));

            buildingPointsLines.Add(new(curve.BuildingPoints[i], curve.BuildingPoints[i+1], variableT));
            buildingPointsLines[^1].GenerateValues();
        }

        return buildingPointsLines;
    }

    private int DetectPoint() {
        Vector2D mouse = (Vector2D)Raylib.GetMousePosition();
        for(int i = 0; i < curve.BuildingPoints.Count; i++) {
            if(Vector2D.Distance(curve.BuildingPoints[i], mouse) < 10) {
                return i;
            }
        }

        return -1;
    }

    // (index of thefirst buildingPoint on the line, Position on the line of the mouse from the click)
    private (int, Vector2D)? DetectLine() {
        Vector2D mouse = (Vector2D)Raylib.GetMousePosition();

        for(int i = 0; i < buildingPointsLines.Count; i++) {
            foreach(Vector2D pointOnLine in buildingPointsLines[i].Values) {
                if(Vector2D.Distance(pointOnLine, mouse) < 10) {
                    return (i, pointOnLine);
                }
            }
        }

        return null;
    }

    public void Update() {
        if(!Editable) {
            return;
        }

        if(Raylib.IsKeyDown(KeyboardKey.LeftControl)) {
            if(Raylib.IsMouseButtonDown(MouseButton.Left)) {
                if(movingPoint == -1) { // setup
                    movingPoint = DetectPoint();
                }
                if(movingPoint != -1) { // Update depending on the quantity of movement (to avoid doing it constently)
                    if(Vector2D.Distance(curve.BuildingPoints[movingPoint], (Vector2D)Raylib.GetMousePosition()) > tolerance) {
                        curve.UpdatePoint((Vector2D)Raylib.GetMousePosition(), movingPoint);
                        this.buildingPointsLines = BuildLines();
                    }
                }
            }
            if(Raylib.IsMouseButtonReleased(MouseButton.Left)) {
                if(movingPoint != -1) { // act and reset
                    curve.UpdatePoint((Vector2D)Raylib.GetMousePosition(), movingPoint);
                    this.buildingPointsLines = BuildLines();
                    movingPoint = -1;
                }
            }
        }
        else if (Raylib.IsKeyReleased(KeyboardKey.LeftControl)) {
            if(movingPoint != -1) { // act and reset
                curve.UpdatePoint((Vector2D)Raylib.GetMousePosition(), movingPoint);
                this.buildingPointsLines = BuildLines();
                movingPoint = -1;
            }
        }
        else if(Raylib.IsKeyDown(KeyboardKey.LeftShift)) {
            if(Raylib.IsMouseButtonPressed(MouseButton.Left)) {
                curve.InsertPoint((Vector2D)Raylib.GetMousePosition(), 0);
                this.buildingPointsLines = BuildLines();
            }
        }
        else if(Raylib.IsMouseButtonPressed(MouseButton.Left)) {
            var pointsOnLine = DetectLine();

            if(pointsOnLine != null) {
                curve.InsertPoint(pointsOnLine.Value.Item2, pointsOnLine.Value.Item1 + 1);
                this.buildingPointsLines = BuildLines();
            }
            else {
                curve.InsertPoint((Vector2D)Raylib.GetMousePosition(), curve.BuildingPoints.Count);
                this.buildingPointsLines = BuildLines();
            }
        }
        else if(Raylib.IsMouseButtonPressed(MouseButton.Right)) {
            int index = DetectPoint();
            if(index >= 0 && index < curve.BuildingPoints.Count) {
                curve.DeletePoint(index);
                this.buildingPointsLines = BuildLines();
            } 
        }
    }
}