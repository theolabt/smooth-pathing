using Raylib_cs;

namespace splines.src.model.space;

abstract class Vector {
    public abstract double Length();
    public abstract Vector Normalize();
    public abstract Vector Round(int decimals = 0);
}

class Vector2D : Vector { 
    public double X { get; set; }
    public double Y { get; set; }

    // constructors
    public Vector2D(double x, double y) {
        X = x;
        Y = y;
    }

    // implicit conversion
    public static implicit operator Vector2D((double x, double y) tuple) {
        return new Vector2D(tuple.x, tuple.y);
    }

    public static explicit operator System.Numerics.Vector2(Vector2D vector) {
        return new System.Numerics.Vector2((float)vector.X, (float)vector.Y);
    }
    public static explicit operator Vector2D(System.Numerics.Vector2 vector) {
        return new Vector2D((float)vector.X, (float)vector.Y);
    }

    // Operations
    public override double Length() => (double)Math.Sqrt(X * X + Y * Y);
    public override Vector2D Normalize() => this / Length();
    public static double Distance(Vector2D A, Vector2D B) => (A - B).Length();

    public static double Dot(Vector2D A, Vector2D B) => A.X * B.X + A.Y * B.Y;
    public static double Dot(Vector2D A, Vector2D B, double angle) => A.Length() * B.Length() * (double)Math.Cos(angle);

    public static Vector2D Lerp(Vector2D A, Vector2D B, double t) 
        => (1 - t) * A + B * t;

    public override Vector2D Round(int decimals = 0) {
        return new Vector2D(Math.Round(X, decimals), Math.Round(Y, decimals));
    }

    // Operator overloads
    public static Vector2D operator -(Vector2D a) => new(-a.X, -a.Y);

    public static Vector2D operator +(Vector2D a, Vector2D b) => new(a.X + b.X, a.Y + b.Y);
    public static Vector2D operator +(Vector2D a, double b) => new(a.X + b, a.Y + b);
    public static Vector2D operator +(double a, Vector2D b) => new(a + b.X, a + b.Y);
    
    public static Vector2D operator -(Vector2D a, Vector2D b) => new(a.X - b.X, a.Y - b.Y);
    public static Vector2D operator -(Vector2D a, double b) => new(a.X - b, a.Y - b);
    public static Vector2D operator -(double a, Vector2D b) => new(a - b.X, a - b.Y);
    
    public static Vector2D operator *(Vector2D a, double b) => new(a.X * b, a.Y * b);
    public static Vector2D operator *(double a, Vector2D b) => new(a * b.X, a * b.Y);
    
    public static Vector2D operator /(Vector2D a, double b) => new(a.X / b, a.Y / b);

    
    public static bool operator ==(Vector2D a, Vector2D b) => a.X == b.X && a.Y == b.Y;
    public static bool operator !=(Vector2D a, Vector2D b) => a.X != b.X || a.Y != b.Y;
    public static bool operator >(Vector2D a, Vector2D b) => a.X > b.X && a.Y > b.Y;
    public static bool operator <(Vector2D a, Vector2D b) => a.X < b.X && a.Y < b.Y;
    public static bool operator >=(Vector2D a, Vector2D b) => a.X >= b.X && a.Y >= b.Y;
    public static bool operator <=(Vector2D a, Vector2D b) => a.X <= b.X && a.Y <= b.Y;
    

    public override bool Equals(object? obj) {
        if (obj is Vector2D) {
            Vector2D other = (Vector2D)obj;
            return this == other;
        }
        return false;
    }
    public override int GetHashCode() {
        return X.GetHashCode() + X.GetHashCode() * Y.GetHashCode();
    }

    public override string ToString() {
        return $"({X}, {Y})";
    }
}

class Vector3D : Vector {
    public double X { get; set; }
    public double Y { get; set; }
    public double Z { get; set; }

    // constructors
    public Vector3D(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
    }

    // implicit conversion
    public static implicit operator Vector3D((double x, double y, double z) tuple) {
        return new Vector3D(tuple.x, tuple.y, tuple.z);
    }

    public static explicit operator System.Numerics.Vector3(Vector3D vector) {
        return new System.Numerics.Vector3((float)vector.X, (float)vector.Y, (float)vector.Z);
    }

    public static explicit operator Vector3D(Color color) {
        return new Vector3D(color.R, color.G, color.B);
    }
    public static explicit operator Color(Vector3D vector) {
        return new Color((int)vector.X, (int)vector.Y, (int)vector.Z);
    }

    // Operations
    public override double Length() => (double)Math.Sqrt(X * X + Y * Y + Z * Z);
    public override Vector3D Normalize() => this / Length();
    public static double Distance(Vector3D A, Vector3D B) => (A - B).Length();

    public static double Dot(Vector3D A, Vector3D B) => A.X * B.X + A.Y * B.Y + A.Z * B.Z;
    public static double Dot(Vector3D A, Vector3D B, double angle) => A.Length() * B.Length() * (double)Math.Cos(angle);

    public static Vector3D Lerp(Vector3D A, Vector3D B, double t) 
        => (1 - t) * A + B * t;

    public override Vector3D Round(int decimals = 0) {
        return new Vector3D(Math.Round(X, decimals), Math.Round(Y, decimals), Math.Round(Z, decimals));
    }

    // Operator overloads
    public static Vector3D operator -(Vector3D a) => new(-a.X, -a.Y, -a.Z);

    public static Vector3D operator +(Vector3D a, Vector3D b) => new(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
    public static Vector3D operator +(Vector3D a, double b) => new(a.X + b, a.Y + b, a.Z + b);
    public static Vector3D operator +(double a, Vector3D b) => new(a + b.X, a + b.Y, a + b.Z);

    public static Vector3D operator -(Vector3D a, Vector3D b) => new(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
    public static Vector3D operator -(Vector3D a, double b) => new(a.X - b, a.Y - b, a.Z - b);
    public static Vector3D operator -(double a, Vector3D b) => new(a - b.X, a - b.Y, a - b.Z);

    public static Vector3D operator *(Vector3D a, double b) => new(a.X * b, a.Y * b, a.Z * b);
    public static Vector3D operator *(double a, Vector3D b) => new(a * b.X, a * b.Y, a * b.Z);

    public static Vector3D operator /(Vector3D a, double b) => new(a.X / b, a.Y / b, a.Z / b);

    public static bool operator ==(Vector3D a, Vector3D b) => a.X == b.X && a.Y == b.Y && a.Z == b.Z;
    public static bool operator !=(Vector3D a, Vector3D b) => a.X != b.X || a.Y != b.Y || a.Z != b.Z;
    public static bool operator >(Vector3D a, Vector3D b) => a.X > b.X && a.Y > b.Y && a.Z > b.Z;
    public static bool operator <(Vector3D a, Vector3D b) => a.X < b.X && a.Y < b.Y && a.Z < b.Z;
    public static bool operator >=(Vector3D a, Vector3D b) => a.X >= b.X && a.Y >= b.Y && a.Z >= b.Z;
    public static bool operator <=(Vector3D a, Vector3D b) => a.X <= b.X && a.Y <= b.Y && a.Z <= b.Z;

    public override bool Equals(object? obj) {
        if (obj is Vector3D other) {
            return this == other;
        }
        return false;
    }

    public override int GetHashCode() {
        return X.GetHashCode() + X.GetHashCode() * Y.GetHashCode() + X.GetHashCode() * Y.GetHashCode() * Z.GetHashCode();
    }

    public override string ToString() {
        return $"({X}, {Y}, {Z})";
    }
}