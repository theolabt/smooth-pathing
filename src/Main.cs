﻿using Raylib_cs;

using splines.src.view;
using splines.src.utils;
using splines.src.model.space;
using splines.src.model.curves;


Raylib.InitWindow(1100, 1100, "Splines");
Raylib.SetTargetFPS(60);


Variable<double> variable = new DoubleVariable(0, 1, 100);

BezierDeCasteljau bezier = new BezierDeCasteljau(variable);
bezier.GenerateValues();

BezierDeCasteljauDisplay display = new BezierDeCasteljauDisplay(bezier);

KeyboardIterator<double> iterator = new(variable);
Editor editor = new Editor(bezier);


Raylib.SetMousePosition(Raylib.GetScreenWidth() / 2, Raylib.GetScreenHeight() / 2);

while (!Raylib.WindowShouldClose()) {
    Raylib.BeginDrawing();
    Raylib.ClearBackground(Constantes.BACKGROUND_COLOR);

    display.Update();

    editor.Update();


    iterator.UpdateValue(Raylib.GetFrameTime());

    // Raylib.DrawCircleV(Raylib.GetMousePosition(), 10, Color.White);
    Raylib.DrawText(Raylib.GetMousePosition().ToString(), Raylib.GetScreenWidth() - 100, Raylib.GetScreenHeight() - 40, 20, new Color(255, 190, 87));

    Raylib.EndDrawing();
}

Raylib.CloseWindow();




