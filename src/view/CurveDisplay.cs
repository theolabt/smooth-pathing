using Raylib_cs;

using splines.src.model.space;
using splines.src.model.curves;
using splines.src.model.functions;

namespace splines.src.view;


class CurveDisplay {
    protected CurveFunction<double, Vector2D> function;
    protected ColorTree colorTree;
    protected Variable<double> variableT;

    public CurveDisplay(CurveFunction<double, Vector2D> function) {
        this.function = function;
        this.colorTree = ColorTreeBuilder.BuildColorTree(function.BuildingPoints.Count, function.VariableX);

        variableT = function.VariableX;
    }

    public virtual void Update() { // TODO need this to be better dealt with
        if(function.BuildingPoints.Count != colorTree.LeafColors.Count) {
            colorTree = ColorTreeBuilder.BuildColorTree(function.BuildingPoints.Count, function.VariableX);
        }

        Draw();
    }

    public virtual void Draw() {
        // curve 
        for(int i = 0; i < variableT.CurrentIndex; i++) {
            Raylib.DrawLineEx((System.Numerics.Vector2)function.Values[i], (System.Numerics.Vector2)function.Values[i + 1], 5, colorTree.ColorRange[i]);
        }
        // building points line
        for(int i = 0; i < function.BuildingPoints.Count - 1; i++) {
            Raylib.DrawLineEx((System.Numerics.Vector2)function.BuildingPoints[i], (System.Numerics.Vector2)function.BuildingPoints[i + 1], 3, colorTree.LeafColors[i]);
        }
        // building points
        foreach(int i in Enumerable.Range(0, function.BuildingPoints.Count)) {
            Raylib.DrawCircleV((System.Numerics.Vector2)function.BuildingPoints[i], 10, colorTree.LeafColors[i]);
        }
        // main point
        Raylib.DrawCircleV((System.Numerics.Vector2)function.Values[variableT.CurrentIndex], 10, colorTree.ColorRange[variableT.CurrentIndex]);

        Raylib.DrawText(
            function.Values[variableT.CurrentIndex].ToString(), 
            20, 
            Raylib.GetScreenHeight()- 40,
            20,
            Color.LightGray
        );
    }
}

class BezierDeCasteljauDisplay : CurveDisplay {
    public BezierDeCasteljauDisplay(BezierDeCasteljau function) : base(function) {}

    private void RecursiveDraw(Function<double, Vector2D> lerp, ColorTree colorTree) {
        if(lerp is Lerp2D) {
            return;
        }

        CompositeLerp2D compositeLerp = (CompositeLerp2D)lerp;

        Raylib.DrawLineEx(
            (System.Numerics.Vector2)compositeLerp.LeftFunction.Values[variableT.CurrentIndex], 
            (System.Numerics.Vector2)compositeLerp.RightFunction.Values[variableT.CurrentIndex], 
            2, 
            colorTree.ColorRange[variableT.CurrentIndex]
        );

        Raylib.DrawCircleV(
            (System.Numerics.Vector2)compositeLerp.LeftFunction.Values[variableT.CurrentIndex], 
            7, 
            colorTree.ColorRange[variableT.CurrentIndex]
        );

        Raylib.DrawCircleV(
            (System.Numerics.Vector2)compositeLerp.RightFunction.Values[variableT.CurrentIndex], 
            7, 
            colorTree.ColorRange[variableT.CurrentIndex]
        );

        RecursiveDraw(compositeLerp.LeftFunction, colorTree.Left!);
        RecursiveDraw(compositeLerp.RightFunction, colorTree.Right!);
    }

    public override void Draw() { 
        BezierDeCasteljau func = (BezierDeCasteljau)this.function;

        RecursiveDraw(
            func.FinalLerp,
            colorTree
        );

        Raylib.DrawText(
            "Bezier - DeCasteljau", 
            20, 
            20,
            20,
            Color.LightGray
        );
        
        base.Draw();
    }
}