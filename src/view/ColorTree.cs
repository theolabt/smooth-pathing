using Raylib_cs;
using splines.src.model.space;

namespace splines.src.view;

//! this way of building the rainbow and the colors is temporary
// it works by building a Bezier curve made of colors (the leafs are the building point in 3D and the root follows the curve)
// the probem being that the curve is way too smooth, and doesn't pass by the building points (exactly like a bezier curve would)
// So this will be changed to use instead a better spline system in the futur, but this is good enough for now 

class ColorTree {
    public ColorTree? Left { get; private set; }
    public ColorTree? Right { get; private set; }

    private HashSet<ColorTree> leafs;

    private List<Color> colorT;
    public Variable<double> VariableT { get ; private set; }

    public Color Color {
        get => colorT[VariableT.CurrentIndex];
    }
    public List<Color> ColorRange {
        get => colorT;
    }
    public List<Color> LeafColors {
        get {
            List<Color> leafColors = new();
            foreach(ColorTree leaf in leafs) {
                leafColors.Add(leaf.Color);
            }
            return leafColors;
        }
    }


    public ColorTree(Color color, Variable<double> variableT) {
        this.VariableT = variableT;

        this.Left = null;
        this.Right = null;

        this.colorT = new();
        foreach(int i in Enumerable.Range(0, variableT.Values.Count)) {
            this.colorT.Add(color);
        }

        this.leafs = new HashSet<ColorTree>() { this };
    }
    public ColorTree(ColorTree left, ColorTree right) {
        if(left.VariableT != right.VariableT) {
            throw new System.Exception("Both trees must have the same variable");
        }

        this.Left = left;
        this.Right = right;

        this.VariableT = left.VariableT;

        this.colorT = new();

        foreach(int i in Enumerable.Range(0, VariableT.Values.Count)) {
            this.colorT.Add((Color)Vector3D.Lerp((Vector3D)left.colorT[i], (Vector3D)right.colorT[i], VariableT.Values[i]));
        }

        this.leafs = left.leafs;
        foreach(ColorTree leaf in right.leafs) {
            this.leafs.Add(leaf);
        }

    }
}

//* Builder
//? color crafted with https://colordesigner.io/color-wheel
static class ColorTreeBuilder {
    private static ColorTree rainbow = BuildColorTree(
        new List<Color>() {
            new(255, 102, 102), // Red
            new(255, 217, 102),  // Yellow
            new(179, 255, 102),  // Green
            new(102, 255, 140),  // Blue Green
            new(102, 255, 255),  // Azure
            new(102, 140, 255),  // Deep Blue
            new(178, 102, 252),  // Purple
            new(255, 102, 217)  // Purple / Pink
        }, 
        new DoubleVariable(0, 1, 200)
    );

    public static ColorTree Rainbow {
        get => rainbow;
    }

    private static ColorTree RecursiveBuildColorTree(List<ColorTree> TreeList, Variable<double> variableT) {
        if(TreeList.Count == 1) {
            return TreeList[0];
        }

        List<ColorTree> newTreeList = new();

        for(int i = 0; i < TreeList.Count - 1; i++) {
            newTreeList.Add(new ColorTree(TreeList[i], TreeList[i + 1]));
        }

        return RecursiveBuildColorTree(newTreeList, variableT);
    }
    public static ColorTree BuildColorTree(List<Color> colorList, Variable<double> variableT) {
        if(colorList.Count == 0) {
            throw new System.Exception("Color list must have at least one color");
        }
        if(colorList.Count == 1) {
            return new ColorTree(colorList[0], variableT);
        }

        List<ColorTree> TreeList = new();
        foreach(Color color in colorList) {
            TreeList.Add(new ColorTree(color, variableT));
        }

        return RecursiveBuildColorTree(TreeList, variableT);
    }

    public static ColorTree BuildColorTree(int nbColors, Variable<double> variableT) {
        List<Color> colorList = new();
        for(int i = 0; i < nbColors; i++) {
            colorList.Add(rainbow.ColorRange[i * (rainbow.VariableT.Values.Count-1) / (nbColors-1)]);
        }


        return BuildColorTree(colorList, variableT);
    }
}