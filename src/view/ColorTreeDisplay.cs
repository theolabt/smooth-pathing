using Raylib_cs;

using splines.src.model.space;

namespace splines.src.view;

class ColorTreeDisplay {
    private ColorTree colorTree;
    private Variable<double> variableT;

    public ColorTreeDisplay(ColorTree colorTree) {
        this.colorTree = colorTree;
        variableT = colorTree.VariableT;
    }

    public void RecursiveDraw(ColorTree colorTree, Vector2D position, int padding) {
        Raylib.DrawCircleV((System.Numerics.Vector2)position, 10, colorTree.ColorRange[variableT.CurrentIndex]);

        if(colorTree.Left == null || colorTree.Right == null) {
            return;
        }

        Raylib.DrawLineEx(
            (System.Numerics.Vector2)position, 
            (System.Numerics.Vector2)(position + new Vector2D(-padding/2, padding)), 
            2, 
            colorTree.Left.ColorRange[variableT.CurrentIndex]
        );

        Raylib.DrawLineEx(
            (System.Numerics.Vector2)position, 
            (System.Numerics.Vector2)(position + new Vector2D(padding/2, padding)), 
            2, 
            colorTree.Right.ColorRange[variableT.CurrentIndex]
        );

        RecursiveDraw(colorTree.Left, position + new Vector2D(-padding/2, padding), padding / 2);
        RecursiveDraw(colorTree.Right, position + new Vector2D(padding/2, padding), padding / 2);
    }

    //! definitely not perfect (and not made to be) 
    // right now points that are the same are drawn separately, when they should be as one
    // there is no real reason for it, appart that I don't need the tree to be correct for now
    public void Draw() {
        RecursiveDraw(colorTree, new Vector2D(Raylib.GetScreenWidth() / 2, 40), Raylib.GetScreenWidth() / 3);

        Raylib.DrawText(
            variableT.CurrentIndex.ToString(), 
            20, 
            Raylib.GetScreenHeight()- 40,
            20,
            Color.LightGray
        );
    }
}